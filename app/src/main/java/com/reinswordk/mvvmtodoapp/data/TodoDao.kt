package com.reinswordk.mvvmtodoapp.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface TodoDao {

    //Insert daftar apa yang dilakukan dan akan replace jika terjadi konflik
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTodo(todo: Todo)

    //delete apa yang dilakukan
    @Delete
    suspend fun deleteTodo(todo: Todo)

    //mendapatkan id
    @Query("SELECT * FROM todo WHERE id = :id")
    suspend fun getTodoById(id: Int): Todo?

    //mengambil list apa yang dilakukan dari todo
    @Query("SELECT * FROM todo")
    fun getTodos(): Flow<List<Todo>>
}