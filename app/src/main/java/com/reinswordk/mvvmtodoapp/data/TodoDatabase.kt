package com.reinswordk.mvvmtodoapp.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [Todo::class],
    version = 1
)
//set database ke roomdatabase
abstract class TodoDatabase: RoomDatabase() {

    abstract val dao: TodoDao
}